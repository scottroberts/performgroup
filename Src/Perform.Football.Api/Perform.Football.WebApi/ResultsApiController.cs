using System;
using System.Web.Http;
using Perform.FootBall.Services;

namespace Perform.Football.WebApi
{
    public class ResultsApiController : ApiController
    {
        private readonly IResultService _resultsService;

        public ResultsApiController(IResultService resultsService)
        {
            if (resultsService == null) throw new ArgumentNullException("resultsService");
            _resultsService = resultsService;
        }

        public bool Post(string matchResult)
        {
            return _resultsService.SaveMatchResult(matchResult);
        }

        public string Get(uint requestedGameWeek)
        {
            if (requestedGameWeek == 0 || requestedGameWeek > 38)
            {
                return "[]";
            }

            return "[{},{}]";
        }
    }
}