﻿using System;
using FakeItEasy;
using NUnit.Framework;
using Perform.Football.WebApi;
using Perform.FootBall.Services;

namespace Perform.Football.WebAPI.Tests
{
    public class ResultsApiControllerTests
    {
        private const string PostMatchResultData = "{ \"gameweek\" : 12, \"homeTeam\": \"Arsenal\", \"awayTeam\":\"Leicester\",\"homeGoals\":2,\"awayGoals\":1}";

        private const string GetMatchWeekData = "[{ \"homeTeam\": \"Arsenal\", \"awayTeam\":\"Aston Villa\",\"homeGoals\":2,\"awayGoals\":0, \"result\" : \"Home Win\"}" +
                                                "{ \"homeTeam\": \"Leicester\", \"awayTeam\":\"Sunderland\",\"homeGoals\":1,\"awayGoals\":1, \"result\" : \"Draw\"}" +
                                                "{ \"homeTeam\": \"Liverpool\", \"awayTeam\":\"Manchester United\",\"homeGoals\":0,\"awayGoals\":1, \"result\" : \"Away Win\"}]";


        private IResultService _fakeResultsService;
        private ResultsApiController _resultsController;

        [SetUp]
        public void TestSetUp()
        {
            _fakeResultsService = A.Fake<IResultService>();
            A.CallTo(() => _fakeResultsService.SaveMatchResult(PostMatchResultData)).Returns(true);
            _resultsController = new ResultsApiController(_fakeResultsService);
        }

        [Test]
        public void ResultsServiceThrowsArgumentNullExceptionWhenNullResultsService()
        {
            Assert.That(() => new ResultsApiController(null), Throws.InstanceOf<ArgumentException>());
        }


        [Test]
        public void PostToResultsReturnsTrue()
        {
            bool result = _resultsController.Post(PostMatchResultData);

            Assert.That( result, Is.True);
        }

        [Test]
        public void PostPassesMatchResultToServiceForProcessing()
        {
            _resultsController.Post(PostMatchResultData);

            A.CallTo(() => _fakeResultsService.SaveMatchResult(PostMatchResultData)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestCase(1, "[{},{}]")]
        [TestCase(5, "[{},{}]")]
        [TestCase(38, "[{},{}]")]
        [TestCase(39, "[]")]
        [TestCase(50, "[]")]
        public void GetResultsForValidGameWeekReturnsAllResults(int requestedGameWeek, string expectedResult )
        {
            var results = _resultsController.Get((uint)requestedGameWeek);

            Assert.That(results, Is.EqualTo(expectedResult));
        }

    }
}
